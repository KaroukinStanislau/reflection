package by.training;

import by.training.task1.Factory;
import by.training.task1.Interface;
import by.training.task1.WithAnnotation;
import by.training.task1.WithoutAnnotation;
import by.training.task2.Analyzer;
import by.training.task2.ForCompare;

public class Runner {
    public static void main(final String[] args) {
        //-----Task1
        System.out.println("---------Task1-------------");
        Factory factory = new Factory();
        Interface t = (Interface) factory.getInstanceOf(new WithAnnotation("Hello from annotated"
                + " class"));
        System.out.println(t.getClass());
        t.someMethod();
        System.out.println();

        t = (Interface) factory.getInstanceOf(new WithoutAnnotation("Hello from class without "
                + "annotation"));
        System.out.println(t.getClass());
        t.someMethod();
        System.out.println("---------Task2-------------");
        //-----Task2

        Analyzer analyzer = new Analyzer();
        ForCompare f1 = new ForCompare();
        ForCompare f2 = new ForCompare();
        System.out.println(f1 + " Equal? " + f2 + " : " + analyzer.equalObjects(f1, f2));
        f2.i2 = 4;
        System.out.println(f1 + " Equal? " + f2 + " : " + analyzer.equalObjects(f1, f2));
        f2.setI(22);
        System.out.println(f1 + " Equal? " + f2 + " : " + analyzer.equalObjects(f1, f2));
    }
}

