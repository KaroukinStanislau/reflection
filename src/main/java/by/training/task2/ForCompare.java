package by.training.task2;

public class ForCompare {
    @Equal(compareby = Equal.Compareby.VALUE)
    private Integer i = new Integer(42);
    public Integer i2 = new Integer(3);

    public void setI(Integer i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return "{i=" + i
                + ", i2=" + i2
                + '}';
    }
}
