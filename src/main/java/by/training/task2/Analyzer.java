package by.training.task2;

import by.training.task2.Equal.Compareby;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Analyzer {
    public boolean equalObjects(final Object obj1, final Object obj2) {
        Class c1 = obj1.getClass();
        Class c2 = obj2.getClass();
        Field[] fields1 = c1.getDeclaredFields();
        //Field[] fields2 = c2.getDeclaredFields();

        if (c1.equals(c2)) {
            for (int i = 0; i < fields1.length; i++) {
                fields1[i].setAccessible(true);
                //fields2[i].setAccessible(true);
                if (hasEqualAnnotation(fields1[i])) {
                    Compareby wayToCompare = fields1[i].getAnnotation(Equal.class).compareby();
                    if (!compareField(wayToCompare, fields1[i], obj1, obj2)) {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
        return true;
    }

    private boolean compareField(final Compareby wayToCompare, final Field field, final Object obj, final
    Object obj2) {
        try {
            if (wayToCompare == Compareby.REFERENCE) {
                return field.get(obj) == field.get(obj2);
            } else if (wayToCompare == Compareby.VALUE) {
                return field.get(obj).equals(field.get(obj2));
            }
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean hasEqualAnnotation(final Field field) {
        Annotation a = field.getAnnotation(Equal.class);
        if (a != null) {
            return true;
        }
        return false;
    }
}
