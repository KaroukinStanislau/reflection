package by.training.task1;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;

public class Factory {

    public Object getInstanceOf(final Object obj) {
        Proxy proxy = obj.getClass().getAnnotation(Proxy.class);
        if (proxy != null) {
            try {
                Constructor<?> constructor = Class.forName(proxy.invocationHandler())
                        .getDeclaredConstructor(Object.class);
                return java.lang.reflect.Proxy.newProxyInstance(
                        obj.getClass().getClassLoader(),
                        obj.getClass().getInterfaces(),
                        (InvocationHandler) constructor.newInstance(obj));
            } catch (NoSuchMethodException | InstantiationException
                    | InvocationTargetException | IllegalAccessException
                    | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return obj;
    }
}
