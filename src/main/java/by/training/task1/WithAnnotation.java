package by.training.task1;

@Proxy(invocationHandler = "by.training.task1.MyHandler")
public class WithAnnotation implements Interface {
    private String string;

    public WithAnnotation(final String s) {
        this.string = s;
    }

    @Override
    public void someMethod() {
        System.out.println(string);
    }
}
