package by.training.task1;

public class WithoutAnnotation implements Interface {
    private String name;

    public WithoutAnnotation(final String name) {
        this.name = name;
    }

    @Override
    public void someMethod() {
        System.out.println(name);
    }
}
