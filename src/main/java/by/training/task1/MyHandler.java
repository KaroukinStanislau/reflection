package by.training.task1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyHandler implements InvocationHandler {
    private Object obj;

    public MyHandler(final Object obj) {
        this.obj = obj;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("your code before: " + method.getName());
        method.invoke(obj, args);
        System.out.println("your code after: " + method.getName());
        return null;
    }
}
